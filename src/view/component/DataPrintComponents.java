package view.component;

public class DataPrintComponents {
	
	/*
	 * DataPrint Components
	 * */
	public static void printTableTitle(String tableTitle) {
		String[] titleTableComponent = {
				"|",
				" ".repeat(41),
				String.format("%25s",tableTitle),
				" ".repeat(40),
				"|\n"
		};
		
		if(tableTitle.length() > 25) {
			titleTableComponent[1] = " ".repeat(33);
			titleTableComponent[2] = String.format("%40s",tableTitle);
			titleTableComponent[3] = " ".repeat(33);
			
		}
		
		
		printBarOpeningDownward(106);
		for(String segment : titleTableComponent) {
			System.out.print(segment);
		}
		printBarOpeningUpward(106);
	}
	public static void printTableHeaders() {
		String[] tableHeaders = {
				String.format("|    %-10s  |", "ID"),
				String.format("|    %-30s\t    |", "Name"),
				String.format("|    %-5s   |", "Course"),
				String.format("| %-1s |", "Year Level"),
				String.format("| %-3s |\n", "Units Enrolled")
			};
		
		for(String segment : tableHeaders) {
			System.out.print(segment);
		}
		printBarOpeningUpward(106);
	}
	public static void printTableRow(String id, String name, String course, int yearLevel, int unitsEnrolled) {
		System.out.print(
				String.format("|  %-10s   |", id)
			);
			System.out.print(
				String.format("|    %-30s\t    |", name)
			);
			System.out.print(
				String.format("|    %-5s    |", course)
			);
			System.out.print(
				String.format("|      %-1s     |", yearLevel)
			);
			System.out.println(
				String.format("|       %-3s      |", unitsEnrolled)
			);
	}
	public static void printStudentCardHeader(String studentID) {
		String[] titleStudentCardComponent = {
				"/",
				"-".repeat(12),
				"[Student ID # " + studentID + "]",
				"-".repeat(12),
				"\\\n"
		};
		
		for(String segment : titleStudentCardComponent) {
			System.out.print(segment);
		}
	}
	public static void printStudentCardBody(String name, String course, int yearLevel, int unitsEnrolled) {
		System.out.println(
				"|  Name\t\t  "
				+ String.format(": %-27s", name)
				+ "\t   |"
			);
			System.out.println(
				"|  Course\t  "
				+ String.format(": %-7s", course)
				+ "\t\t\t   |"
			);
			System.out.println(
				"|  Year Level     "
				+ String.format(": %-7s", yearLevel)
				+ "\t\t\t   |"
			);
			System.out.println(
				"|  Units Enrolled "
				+ String.format(": %-7s", unitsEnrolled)
				+ "\t\t\t   |"
			);
	}
	
	/*
	 * DataPrint Subcomponents
	 * */
	/** 
	 * Looks something like this:
	 *	/---------------------------\
	 * */
	public static void printBarOpeningDownward(int length) {
		System.out.println("/" + "-".repeat(length) + "\\");
	}
	/** 
	 * Looks something like this:
	 *	\---------------------------/
	 * */
	public static void printBarOpeningUpward(int length) {
		System.out.println("\\" + "-".repeat(length) + "/");
	}
	/** 
	 * Looks something like this:
	 * ==================================
	 * */
	public static void printBar(int length) {
		System.out.println("=".repeat(length));
	}

}
