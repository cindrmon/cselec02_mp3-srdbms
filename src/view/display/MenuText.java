package view.display;

public class MenuText {
	
	/*
	 * DISPLAY MESSAGES
	 * */
	
	public static void welcomeMessage() {
		System.out.println();
		System.out.println("Welcome to the iAcademy Student RDBMS v1.0.0");
		System.out.println("Type the command to execute.");
		System.out.println("Type 'help' for all commands.");
		System.out.println();
	}
	
	public static void welcomeAdminMessage(String adminName) {
		System.out.println();
		System.out.println("Welcome to the iAcademy Admin Student RDBMS v1.0.0");
		System.out.printf("Welcome '%s'!\n", adminName);
		System.out.println("Type the command to execute.");
		System.out.println("Type 'help' for all commands.");
		System.out.println();
	}
	
	public static void exitMessage() {
		System.out.println();
		System.out.println("Program Terminated!");
		System.out.println("Thank you for using this program!");
	}
	
	public static void invalidLoginMessage() {
		System.out.println();
		System.err.println("Incorrect Username and/or Password. Please Try again.");
		System.out.println();
	}
	
	public static void loggingInAsAdminMessage() {
		System.out.println();
		System.out.println("Logging in as admin...");
		System.out.println();
	}
	
	public static void loggedInAsAdminMessage() {
		System.out.println();
		System.out.println("Successfully Logged In!");
		System.out.println();
	}
	
	public static void loggedInAsAdminReminder(String adminName) {
		System.out.println();
		System.out.println("Currently Logged in as " + adminName);
		System.out.println();
	}
	
	public static void loggedOutFromAdmin() {
		System.out.println();
		System.out.println("You have Logged out from the Admin System.");
		System.out.println();
	}
	
	public static void invalidInputMessage() {
		System.out.println();
		System.err.println("Invalid Input.");
		System.out.println("Type 'help' to list all commands.");
		System.out.println();
	}
	
	public static void insufficientPermsMessage() {
		System.out.println();
		System.err.println("You do not have enough permissions to run this command.");
		System.out.println("To login as admin, type 'login'.");
		System.out.println();
	}
	
	/*
	 * DISPLAY AVAILABLE COMMANDS
	 * */
	
	public static void standardCommands() {
		System.out.println();
		System.out.println("Standard Commands List");
		System.out.println();
		System.out.println("'add' -> Adds Student");
		System.out.println("'list' -> List All Student Records");
		System.out.println("'search' -> Lookup a Student through Student ID");
		System.out.println("'help' -> Prints this for all available commands");
		System.out.println("'login' -> Login as admin and access admin-only commands");
		System.out.println();
	}
	
	public static void adminCommands() {
		System.out.println();
		System.out.println("** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **");
		System.out.println("** ** ** ** ** **      Admin Only Commands      ** ** ** ** ** **");
		System.out.println("** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **");
		System.out.println("** 'delete'\t -> Delete Single Student Record\t       **");
		System.out.println("** 'report-gen'  -> Trigger Report Generator\t\t       **");
		System.out.println("** 'help'\t -> Prints this for all available commands.    **");
		System.out.println("** 'purge'\t -> Purge all Student Data\t\t       **");
		System.out.println("** 'logout'\t -> Logout as Admin\t\t\t       **");
		System.out.println("** 'exit'/'quit' -> Terminates the Program\t\t       **");
		System.out.println("** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** **");
		System.out.println();
		System.out.println("Standard Commands List");
		System.out.println();
		System.out.println("'add' -> Adds Student");
		System.out.println("'list' -> List All Student Records");
		System.out.println("'search' -> Lookup a Student through Student ID");
		System.out.println("'help' -> Prints this for all available commands");
		System.out.println();
	}
}






























