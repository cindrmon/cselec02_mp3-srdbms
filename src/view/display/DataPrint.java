package view.display;

import java.sql.ResultSet;
import java.sql.SQLException;

import model.StudentBean;
import util.Security;
import view.component.DataPrintComponents;

public class DataPrint extends DataPrintComponents {

	/*
	 * Display Tables 
	 * */
	/**
	 * Prints all student records from database
	 * 
	 * @param records -> a single ResultSet containing database data
	 * */
	public static void displayStudentRecordList(ResultSet records) {
			try {
				if (records.next() != false) {
					System.out.println("\nData from Database Loaded... Printing all Student Records...\n");
					
					// STUDENT LIST TABLE
					printTableTitle("CURRENT STUDENT LIST");
					printTableHeaders();
					do {
						printTableRow(
								Security.decrypt(records.getString("id")),
								Security.decrypt(records.getString("name")),
								records.getString("course"),
								records.getInt("yearLevel"),
								records.getInt("unitsEnrolled")
								);
					} while(records.next());
					printBarOpeningUpward(106);
					
					// STUDENT STATISTICS
					printBar(100);
					System.out.println("Student Statistics: \n");
					System.out.println("    Total # BS CS Students: " + StudentBean.numberOfBSCS);
					System.out.println("    Total # BS IT Students: " + StudentBean.numberOfBSIT);
					System.out.println("    Total # BS IS Students: " + StudentBean.numberOfBSIS);
					System.out.println("\n    Total Students Enrolled: " + StudentBean.totalNumberOfStudents);
					printBar(100);
					
					System.out.println();
					
				} 
				// if records are empty
				else {
					ProgramMessages.emptyRecordsMessage();
				}
			} catch (SQLException sqlExc) {
				ProgramMessages.sqlExceptionMessage(sqlExc);
			}
	}
	/**
	 * Prints student records that are dependent on requested course filters only.
	 * 
	 * @param records -> a single ResultSet containing database data
	 * @param courseFilter -> used to filter the records to only that particular student course
	 * */
	public static void generateReport(ResultSet records, String courseFilter) {
		try {
			if(records.next() != false) {
				System.out.println("\nData from Database Loaded... Generating Report...\n");
				
				// STUDENT LIST TABLE
				printTableTitle("GENERATED REPORT FOR " + courseFilter + " STUDENTS");
				printTableHeaders();
				do {
					printTableRow(
							Security.decrypt(records.getString("id")),
							Security.decrypt(records.getString("name")),
							records.getString("course"),
							records.getInt("yearLevel"),
							records.getInt("unitsEnrolled")
							);
				} while(records.next());
				printBarOpeningUpward(106);
				
				// STUDENT STATISTICS
				printBar(100);
				System.out.println("Student Statistics: \n");
				if(courseFilter.equals("BS CS"))
					System.out.println("    Total # BS CS Students: " + StudentBean.numberOfBSCS);
				else if(courseFilter.equals("BS IT"))
					System.out.println("    Total # BS IT Students: " + StudentBean.numberOfBSIT);
				else if (courseFilter.equals("BS IS"))
					System.out.println("    Total # BS IS Students: " + StudentBean.numberOfBSIS);
				else {
					System.out.println("    Total # BS CS Students: " + StudentBean.numberOfBSCS);
					System.out.println("    Total # BS IT Students: " + StudentBean.numberOfBSIT);
					System.out.println("    Total # BS IS Students: " + StudentBean.numberOfBSIS);
					System.out.println("\n    Total Students Enrolled: " + StudentBean.totalNumberOfStudents);
				}
				printBar(100);
				
				System.out.println();
				
			}
			// if records are empty
			else {
				ProgramMessages.emptyRecordsMessage();
			}
			
			
		} catch (SQLException sqlExc) {
			ProgramMessages.sqlExceptionMessage(sqlExc);
		}
	}

	/**
	 * Display Single Student Card
	 * 
	 * @param record -> pass in a single record from the ResultSet
	 * @param studentID -> the student id it is requesting to get only that particular record
	 * @param isException -> tells whether if this method is called by an exception handler or not
	 * */
	public static void displayStudentRecord(ResultSet record, String studentID, boolean isException) {
		try {
			if (record.next() != false) {
				// will only print this message if it is not called by an exception handler
				if(!isException) {
					ProgramMessages.recordSearchSuccessMessage();
				}
				printStudentCardHeader(Security.decrypt(record.getString("id")));
				printStudentCardBody(
						Security.decrypt(record.getString("name")),
						record.getString("course"),
						record.getInt("yearLevel"),
						record.getInt("unitsEnrolled")
						);
				printBarOpeningUpward(50);
			} else {
				ProgramMessages.emptyRecordMessage(studentID);
			}
		} catch (SQLException sqlExc) {
			ProgramMessages.sqlExceptionMessage(sqlExc);
		}
	}

}
































