package view.display;

import java.sql.ResultSet;
import java.sql.SQLException;

import model.StudentBean;

/**
 * Contains all possible alert messages for this particular program.
 * 
 * It is designed this way to have easier access to each particular message, and can be edited globally in any way.
 * */
public class ProgramMessages {

	/*
	 * Issue Handling Messages
	 * */
	/* Exception Handling Messages */
	public static void sqlExceptionMessage(SQLException sqlExc) {
		System.out.println();
		System.err.println("Cannot Connect to Database..." + sqlExc.getMessage());
		System.out.println();
	}
	public static void sqlDuplicateIDFoundMessage(SQLException sqlExc, String matchedID) {
		if(sqlExc.getErrorCode() == 1062) {
			System.out.println();
			System.out.println("Record Insertion Error.");
			System.out.println("There is already a Student ID with " + matchedID + ".");
			System.out.println();
			System.out.println("This is the current student with Student ID " + matchedID);
			ResultSet record = new StudentBean().getStudentById(matchedID);
			DataPrint.displayStudentRecord(record, matchedID, true);
			System.out.println();
		}
		else {
			sqlExceptionMessage(sqlExc);
		}
	}
	public static void classNotFoundExceptionMessage(ClassNotFoundException cnfExc) {
		System.out.println();
		System.err.println("Missing JDBC Driver... " + cnfExc.getMessage());
	}
	public static void invalidConnectionMessage() {
		System.out.println();
		System.out.println("Connection made is invalid.");
		System.out.println("Please contact your system administrator or try again later...");
		System.out.println();
	}
	/* Miscellaneous Handling Messages */
	public static void emptyRecordsMessage() {
		System.out.println();
		System.out.println("Records are Empty...");
		System.out.println();
	}
	public static void emptyRecordMessage(String studentID) {
		System.out.println();
		System.out.println("No records found for " + studentID + "...");
		System.out.println();
	}
	public static void studentIdWrongPatternMessage() {
		System.out.println("Invalid Pattern!");
		System.out.println("Student ID must match these Patterns:");
		System.out.println("(200*-******)\n(201*-******)\n(202*-******)");
	}
	public static void studentNameWrongPatternMessage() {
		System.out.println("Name should not have numbers or hanging spaces in it!");
	}
	public static void studentCourseWrongPatternMessage() {
		System.err.println("Invalid Pattern!");
		System.out.println("Available Student Courses are (BS CS, BS IS, and BS IT)");
	}
	public static void studentYearLevelWrongPatternMessage() {
		System.err.println("Invalid Value! Must be between 1 and 4");
	}
	public static void studentUnitsEnrolledWrongPatternMessage() {
		System.err.println("Entered Input is not within the valid range (12-21)");
	}
	
	/*
	 * Success Messages
	 * */
	public static void recordAddSuccessMessage() {
		System.out.println();
		System.out.println("Record Successfully Added!");
		System.out.println();
	}
	public static void recordSearchSuccessMessage() {
		System.out.println("\nRecord Found!");
	}
	public static void recordDeletionSuccessMessage(String selectedID) {
		System.out.println();
		System.out.println("Student ID # " + selectedID + " successfully deleted!");
		System.out.println();
	}
	public static void purgeSuccessMessage() {
		System.out.println();
		System.out.println("All Student Records Successfully Deleted!");
		System.out.println();
	}
	public static void purgeCancelSuccessMessage() {
		System.out.println("\nPurging Progress Cancelled...\n");
	}
	
	/*
	 * Warning/Loading Messages
	 * */
	public static void loadingFromDatabaseMessage() {
		System.out.println("\nPlease Wait... Loading Requested Data from Database... ");
	}
	public static void loadingStudentRecordMessage(String studentID) {
		System.out.println("\nPlease Wait... Searching for Student Record " + studentID);
	}
	public static void loadingDeleteAllRecordsMessage() {
		System.out.println("\nPlease Wait as we are deleting All Records...");
	}
	public static void warningDeleteAllRecordsMessage() {
		System.out.println("\nWARNING! You are about to delete ALL RECORDS from the database!");
	}
}
