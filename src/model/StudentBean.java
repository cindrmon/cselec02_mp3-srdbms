package model;

import java.io.Serializable;
import util.DatabaseQuery;
import util.Security;
import view.display.ProgramMessages;

import java.sql.*;

public class StudentBean implements Serializable, DatabaseQuery {

	private static final long serialVersionUID = 2391124742604652511L;
	
	// variables to be used
	private String id;
	private String name;
	private String course;
	private int yearLevel;
	private int unitsEnrolled;
	
	// static counter numbers
	public static int totalNumberOfStudents = 0;
	public static int numberOfBSCS = 0;
	public static int numberOfBSIT = 0;
	public static int numberOfBSIS = 0;
	
	// Constructor
	public StudentBean() {
		id = "";
		name = "";
		course = "";
		yearLevel = 0;
		unitsEnrolled = 0;
	}
	
	// Setters/Getters
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public int getYearLevel() {
		return yearLevel;
	}

	public void setYearLevel(int yearLevel) {
		this.yearLevel = yearLevel;
	}

	public int getUnitsEnrolled() {
		return unitsEnrolled;
	}

	public void setUnitsEnrolled(int unitsEnrolled) {
		this.unitsEnrolled = unitsEnrolled;
	}
	
	/* JDBC MySQL Server Connection */
	private Connection getConnection() {
		// set default connection to null
		Connection connection = null;
		
		try {
			// JDBC Driver Enrollment
			Class.forName("com.mysql.jdbc.Driver");
			
			/* DriverManager configuration */
			// connection to https://cindrmons-zen.codes (production server)
			connection = DriverManager.getConnection(
				"jdbc:mysql://103.3.61.87:13306/seg21-db",
				"gdfrancia",
				"SomethingComforting517^!*@"
			);
			
			// connection to localhost XAMPP (fallback server)
			// (if in case main production server doesn't work)
			// connection = DriverManager.getConnection(
			// 		"jdbc:mysql://localhost:3306/seg21-db",
			// 		"root",
			// 		""
			// );
			
			// DEBUG-FEATURE: check if connection is valid or invalid
			assert connection != null : "\nValid Connection!\n";
			assert connection == null : "\nInvalid Connection!\n";

			
		} catch(ClassNotFoundException cnfExc) {
			ProgramMessages.classNotFoundExceptionMessage(cnfExc);
		} catch(SQLException sqlExc) {
			ProgramMessages.sqlExceptionMessage(sqlExc);
		}
		
		return connection;
		
	}
	
	/* JDBC CRUD Operations */

	// CREATE Functions
	public void addStudentRecord() {
		Connection ct = null;
		
		try {
			
			ct = getConnection();
			
			// if connection is valid
			if(ct != null) {
				ct.setAutoCommit(false);
				PreparedStatement prepStmt = ct.prepareStatement(DatabaseQuery.ADD_STUDENT);
			
				// Set Appropriate Fields
				prepStmt.setString(1, Security.encrypt(this.id));
				prepStmt.setString(2, Security.encrypt(this.name));
				prepStmt.setString(3, this.course);
				prepStmt.setInt(4, this.yearLevel);
				prepStmt.setInt(5, this.unitsEnrolled);
				
				// Save to DB and return successful operation
				prepStmt.executeUpdate();
				ct.commit();
				ProgramMessages.recordAddSuccessMessage();
			
			}
			// if connection is invalid
			else {
				ProgramMessages.invalidConnectionMessage();
			}
			
		} catch(SQLException sqlExc) {
			
			try {
				ct.rollback();
				ProgramMessages.sqlDuplicateIDFoundMessage(sqlExc, this.id);
			} catch (SQLException sqlExc2) {
				ProgramMessages.sqlExceptionMessage(sqlExc2);
			}
			
			
		}
		
	}
	
	// READ Functions
	public ResultSet getAllStudents() {
		ResultSet records = null;
		
		try {
			Connection ct = getConnection();
			
			// Validate Connection
			if(ct != null) {
				PreparedStatement prepStmt = ct.prepareStatement(DatabaseQuery.LIST_ALL_STUDENTS);
				records = prepStmt.executeQuery();
					
			}
			// if connection is invalid
			else {
				ProgramMessages.invalidConnectionMessage();
			}
			
		} catch(SQLException sqlExc) {
			ProgramMessages.sqlExceptionMessage(sqlExc);
		}
		
		getStudentCount();
		return records;
	}
	
	public ResultSet getStudentByCourse(String courseFilter) {
		ResultSet records = null;
		
		
		try {
			Connection ct = getConnection();
			if(ct != null) {
				PreparedStatement prepStmt = null;
				
				if(courseFilter.equalsIgnoreCase("all"))
					prepStmt = ct.prepareStatement(DatabaseQuery.LIST_ALL_STUDENTS);
				else {
					prepStmt = ct.prepareStatement(DatabaseQuery.LIST_STUDENTS_BY_COURSE_CODE);
					prepStmt.setString(1, courseFilter);
				}
				
				records = prepStmt.executeQuery();
			}
			// if connection is invalid
			else {
				ProgramMessages.invalidConnectionMessage();
			}
		} catch(SQLException sqlExc) {
			ProgramMessages.sqlExceptionMessage(sqlExc);
		}
		
		getStudentCount();
		return records;
	}
	
	public ResultSet getStudentById(String selectedId) {
		ResultSet records = null;
		
		try {
			Connection ct = getConnection();
			if(ct != null) {
				PreparedStatement prepStmt = ct.prepareStatement(DatabaseQuery.LIST_STUDENT);
				prepStmt.setString(1, Security.encrypt(selectedId));
				records = prepStmt.executeQuery();
				
			} // if connection is invalid
			else {
				ProgramMessages.invalidConnectionMessage();
			}
		} catch(SQLException sqlExc) {
			ProgramMessages.sqlExceptionMessage(sqlExc);
		}

		return records;
	}
	
	private void getStudentCount() {
		ResultSet recordCounter = null;
		PreparedStatement prepStmt = null;
		
		try {
			Connection ct = getConnection();
			
			// Validate Connection
			if(ct != null) {
				
				// Database Query Counts: 
				

				// 0. All Students
				prepStmt = ct.prepareStatement(DatabaseQuery.COUNT_ALL_STUDENTS);
				recordCounter = prepStmt.executeQuery();
				while(recordCounter.next()) {
					totalNumberOfStudents = recordCounter.getInt("totalStudentCount");
				}
	
				// 1. All BS CS Students
				prepStmt = ct.prepareStatement(DatabaseQuery.COUNT_ALL_BSCS);
				recordCounter = prepStmt.executeQuery();
				recordCounter.next();
				numberOfBSCS = recordCounter.getInt("totalBSCS");
				
				// 2. All BS IT Students
				prepStmt = ct.prepareStatement(DatabaseQuery.COUNT_ALL_BSIT);
				recordCounter = prepStmt.executeQuery();
				recordCounter.next();
				numberOfBSIT = recordCounter.getInt("totalBSIT");
				
				// 3. All BS IS Students
				prepStmt = ct.prepareStatement(DatabaseQuery.COUNT_ALL_BSIS);
				recordCounter = prepStmt.executeQuery();
				recordCounter.next();
				numberOfBSIS = recordCounter.getInt("totalBSIS");
				
			}
			// if connection is invalid
			else {
				System.out.println("Cannot get student count...");
			}
			
		} catch(SQLException sqlExc) {
			ProgramMessages.sqlExceptionMessage(sqlExc);
		}
		
	}
	

	// DELETE Functions
	public void deleteStudentRecord(String selectedId) {
		Connection ct = null;
		
		
		try {
			ct = getConnection();
			ResultSet record = getStudentById(selectedId);
			
			if(ct != null) {
				ct.setAutoCommit(false);
				
				if(record.next() != false) {
					PreparedStatement prepStmt = ct.prepareStatement(DatabaseQuery.DELETE_STUDENT);
					
					prepStmt.setString(1, Security.encrypt(selectedId));
					
					prepStmt.executeUpdate();
					ct.commit();
					ProgramMessages.recordDeletionSuccessMessage(selectedId);
				}
				else {
					ct.rollback();
					ProgramMessages.emptyRecordMessage(selectedId);
				}
			}
			
			// if connection is invalid
			else {
				ProgramMessages.invalidConnectionMessage();
			}
		} catch(SQLException sqlExc) {
			try {
				ct.rollback();
				ProgramMessages.sqlExceptionMessage(sqlExc);
			} catch(SQLException sqlExc2) {
				ProgramMessages.sqlExceptionMessage(sqlExc2);
			}
			
		};
		
	}
	
	public void deleteAllRecords() {
		Connection ct = null;
		try {
			ct = getConnection();
			ResultSet records = getAllStudents();
			
			if(ct != null) {
				ct.setAutoCommit(false);
				
				PreparedStatement prepStmt = ct.prepareStatement(DatabaseQuery.DELETE_ALL);
				
				if(records.next() != false) {
					prepStmt.executeUpdate();
					ct.commit();
					ProgramMessages.purgeSuccessMessage();
				}
				else {
					ct.rollback();
					ProgramMessages.emptyRecordsMessage();
				}
			}
			// if connection is invalid
			else {
				ProgramMessages.invalidConnectionMessage();
			}
			
		} catch(SQLException sqlExc) {
			try {
				ct.rollback();
				ProgramMessages.sqlExceptionMessage(sqlExc);
			} catch (SQLException sqlExc2) {
				ProgramMessages.sqlExceptionMessage(sqlExc2);
			}
			
		}
		
		
	}

}
