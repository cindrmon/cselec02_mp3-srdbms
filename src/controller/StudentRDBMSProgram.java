package controller;

import util.KBInput;
import view.display.MenuText;


public class StudentRDBMSProgram {
	
	// Hardcoded Admin Account 
	/* 
	 * I know this is not recommended for security,
	 * but this is just for educational purposes
	 * only..
	*/
	private static String adminUsername = "admin";
	private static String adminPassword = "qwerty1234";
	
	// User Admin Input
	private static String usernameInput = "";
	private static String passwordInput = "";
	
	private static String choice = "";
	

	public static void main(String[] args) {
		
		String[] adminOnlyCommands = {
				"exit",
				"quit",
				"delete",
				"report-gen",
				"purge"
		};
		boolean isAdminCommand = false;
		
		MenuText.welcomeMessage();
		MenuText.standardCommands();
		
		while(true) {
			
			choice = KBInput.readString("> ");
			
			// checks if user runs an admin-only command
			// outside the admin login
			for(String cmd : adminOnlyCommands) {
				if(cmd.equalsIgnoreCase(choice)) {
					isAdminCommand = true;
					break;
				}
			}
			if(isAdminCommand) {
				MenuText.insufficientPermsMessage();
				isAdminCommand = false;
			}
			
			else if(choice.equalsIgnoreCase("help")) {
				MenuText.standardCommands();
			}
			
			else if(choice.equalsIgnoreCase("login")) {
				// if adminMain() is true,
				// it will stop main loop
				if (adminMain()) break;
			}
			
			else if(choice.equalsIgnoreCase("add")) {
				StudentController.addStudentToDB();
			}
			
			else if(choice.equalsIgnoreCase("list")) {
				StudentController.listAllStudents();
			}
			
			else if(choice.equalsIgnoreCase("search")) {
				StudentController.searchStudentById();
			}
			
			else {
				MenuText.invalidInputMessage();
			}
			
		}
		
		MenuText.exitMessage();

	}

	// Admin-Only Commands
	/* adminMain() returns boolean so that only the admin
	 * can break the main loop when exiting the adminMain()
	 * loop
	 */
	public static boolean adminMain() {
		
		// admin login
		MenuText.loggingInAsAdminMessage();
		usernameInput = KBInput.readString("Enter Admin Username: ");
		passwordInput = KBInput.readString("Enter Password: ");

		
		// if admin successfully logs in
		if(adminUsername.equals(usernameInput) && adminPassword.equals(passwordInput)) {
			MenuText.loggedInAsAdminMessage();
			MenuText.welcomeAdminMessage(usernameInput);
			MenuText.adminCommands();
			
			while(true) {
				
				MenuText.loggedInAsAdminReminder(usernameInput);
				choice = KBInput.readString("# ");
				
				// breaks out of the admin loop, thus logging out
				// the admin user
				if(choice.equalsIgnoreCase("logout")) {
					MenuText.loggedOutFromAdmin();
					MenuText.welcomeMessage();
					MenuText.standardCommands();
					break;
				}
				// passes through the break command to quit the program to the
				// main loop by returning true
				else if(choice.equalsIgnoreCase("exit") || choice.equalsIgnoreCase("quit")) {
					return true;
				}
				else if(choice.equalsIgnoreCase("help")) {
					MenuText.adminCommands();
				}
				else if(choice.equalsIgnoreCase("add")) {
					StudentController.addStudentToDB();
				}
				else if(choice.equalsIgnoreCase("list")) {
					StudentController.listAllStudents();
				}
				else if(choice.equalsIgnoreCase("search")) {
					StudentController.searchStudentById();
				}
				else if(choice.equalsIgnoreCase("delete")) {
					StudentController.deleteStudent();
				}
				else if(choice.equalsIgnoreCase("report-gen")) {
					StudentController.generateReport();
				}
				else if(choice.contentEquals("purge")) {
					StudentController.deleteAllRecords();
				}
				
				else {
					MenuText.invalidInputMessage();
				}
				
			}
			
		} 
		// if username or password or both is invalid
		else {
			MenuText.invalidLoginMessage();
			MenuText.standardCommands();
		}
		return false;
	}	

		
	
}
