package controller;

import util.*;
import view.display.DataPrint;
import view.display.ProgramMessages;

import java.sql.ResultSet;
import model.StudentBean;

public class StudentController {
		
	/* ****************************************************** */
	
	/* Input Validation Methods */
	
	// Student ID Validation
	private static String enterStudentId() {
		String studentID = "";
		System.out.println("Enter Student ID");
		System.out.println("Allowed Patterns:\n(200*-******)\n(201*-******)\n(202*-******)");
		studentID = KBInput.readString("Input Here: ");
		while(!studentID.matches("20[0-2][0-9]-[0-9]{6}")) {
			ProgramMessages.studentIdWrongPatternMessage();
			studentID = KBInput.readString("Enter Student ID: ");
		}
		return studentID;
	}
	private static String enterStudentId(String task) {
		String studentID = "";
		System.out.println("Enter Student ID to " + task);
		System.out.println("Allowed Patterns:\n(200*-******)\n(201*-******)\n(202*-******)");
		studentID = KBInput.readString("Input Here: ");
		while(!studentID.matches("20[0-2][0-9]-[0-9]{6}")) {
			ProgramMessages.studentIdWrongPatternMessage();
			studentID = KBInput.readString("Enter Student ID: ");
		}
		return studentID;
	}
	
	// Name Validation
	private static String enterFullName() {
		String studentFirstName = "";
		String studentLastName = "";
		// Last Name Validation
		studentLastName = KBInput.readString("Enter Student Last Name: ");
		while(!studentLastName.matches("^^\\w(( ?[A-Za-z-]+)?)+$")) {
			ProgramMessages.studentNameWrongPatternMessage();
			studentLastName = KBInput.readString("Enter Student Last Name: ");
		}
		// First Name Validation
		studentFirstName = KBInput.readString("Enter Student First Name: ");
		while(!studentFirstName.matches("^^\\w(( ?[A-Za-z-]+)?)+$")) {
			ProgramMessages.studentNameWrongPatternMessage();
			studentFirstName = KBInput.readString("Enter Student First Name: ");
		}
		// combines First Name and Last Name
		return TextManipulation.toPascalCase(studentLastName) + ", " + TextManipulation.toPascalCase(studentFirstName);
		
	}
	
	// Student Course Validation
	private static String enterStudentCourse() {
		String studentCourse = "";
		studentCourse = KBInput.readString("Enter Student Course (BS CS, BS IS, BS IT): ");
		while(!studentCourse.matches("^(BS CS|BS IT|BS IS)$")) {
			ProgramMessages.studentCourseWrongPatternMessage();
			studentCourse = KBInput.readString("Enter Student Course (BS CS, BS IS, BS IT): ");
		}
		return studentCourse;
	}
	private static String enterCourseFilters() {
		String courseFilters;
		System.out.println("Available Course Code Filters Criteria (BS CS, BS IS, BS IT, All)");
		courseFilters = KBInput.readString("Enter Criteria: ");
		while(!courseFilters.matches("^(BS CS|BS IT|BS IS|[Aa][Ll]{2})$")) {
			ProgramMessages.studentCourseWrongPatternMessage();
			courseFilters = KBInput.readString("Enter Criteria: ");
		}
		return courseFilters;
	}
	
	// Year Level Validation
	private static int enterYearLevel() {
		String studentYearLevel = "";
		studentYearLevel = KBInput.readString("Enter Year Level (1-4): ");
		while(!studentYearLevel.matches("^[1-4]+$")) {
			ProgramMessages.studentYearLevelWrongPatternMessage();
			studentYearLevel = KBInput.readString("Enter Year Level (1-4): ");
		}
		return Integer.parseInt(studentYearLevel);
	}
	
	// Units Enrolled Validation
	private static int enterUnitsEnrolled() {
		int studentUnitsEnrolled = 0;

		studentUnitsEnrolled = KBInput.readInt("Enter Units Enrolled (12-21): ");
		while(studentUnitsEnrolled < 12 || studentUnitsEnrolled > 21) {
			ProgramMessages.studentUnitsEnrolledWrongPatternMessage();
			studentUnitsEnrolled = KBInput.readInt("Enter Units Enrolled (12-21): ");
		}

		return studentUnitsEnrolled;
	}
	
	/* ****************************************************** */
	
	/* CREATE Methods */
	public static void addStudentToDB() {
		
		StudentBean newStudent = new StudentBean();
		
		newStudent.setId(enterStudentId());
		newStudent.setName(enterFullName());
		newStudent.setCourse(enterStudentCourse());
		newStudent.setYearLevel(enterYearLevel());
		newStudent.setUnitsEnrolled(enterUnitsEnrolled());
	
		newStudent.addStudentRecord();
	}

	////////////////////////////////////////////////////////////
	
	/* READ Methods */
	public static void listAllStudents() {
		ProgramMessages.loadingFromDatabaseMessage();
		DataPrint.displayStudentRecordList(new StudentBean().getAllStudents());
	}

	public static void searchStudentById() {
		
		String id = enterStudentId("search");
		
		ProgramMessages.loadingStudentRecordMessage(id);
		DataPrint.displayStudentRecord(new StudentBean().getStudentById(id), id, false);
	}

	public static void generateReport() {
		
		System.out.println("Report Generator Facility\n");
		String course = enterCourseFilters();
		
		ProgramMessages.loadingFromDatabaseMessage();
		if(course.equalsIgnoreCase("all"))
			course = course.toUpperCase();
		DataPrint.generateReport(new StudentBean().getStudentByCourse(course), course);
		
	}

	////////////////////////////////////////////////////////////
	
	/* DELETE Methods */
	public static void deleteStudent() {
		
		String id = enterStudentId("delete");
		
		ProgramMessages.loadingStudentRecordMessage(id);
		new StudentBean().deleteStudentRecord(id);
	
	}
	
	public static void deleteAllRecords() {
		String finalChoice = "";
		System.out.println();
		ProgramMessages.warningDeleteAllRecordsMessage();
		finalChoice = KBInput.readString("Are you sure you want to continue? [Y/n] ");
		if(finalChoice.equalsIgnoreCase("y")) {
			ProgramMessages.loadingDeleteAllRecordsMessage();
			new StudentBean().deleteAllRecords();				
		}
		else {
			ProgramMessages.purgeCancelSuccessMessage();
		}
		
	}

	/* ****************************************************** */
	
}
