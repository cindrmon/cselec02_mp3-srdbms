package util;

/** 
 * When implemented, it must contain all of these database queries for all CRUD operations to work to its respective database.
 * 
 * This is somewhat retrofitted to only work with `seg21-db`.`Student` Table, and nothing else.
 * */
public interface DatabaseQuery {
	
	/* CRUD Functionality */
	
	/* Create Functions */
	// [A]dd Student
	String ADD_STUDENT = "INSERT INTO Student "
		+ "(id, name, course, yearLevel, unitsEnrolled) "
		+ "VALUES (?,?,?,?,?)";
	
	
	
	/* Read Functions */
	// [L]ist Students
	String LIST_ALL_STUDENTS = "SELECT * FROM Student";
	
	// Student Counters
	String COUNT_ALL_STUDENTS = "SELECT COUNT(*) AS 'totalStudentCount' FROM Student";
	String COUNT_ALL_BSCS = "SELECT COUNT(course) AS 'totalBSCS' FROM Student WHERE course = 'BS CS';";
	String COUNT_ALL_BSIT = "SELECT COUNT(course) AS 'totalBSIT' FROM Student WHERE course = 'BS IT';";
	String COUNT_ALL_BSIS = "SELECT COUNT(course) AS 'totalBSIS' FROM Student WHERE course = 'BS IS';";
	
	// [S]earch Student
	String LIST_STUDENT = "SELECT * FROM Student "
		+ "WHERE id = ?";
	
	// [R]eport Generator
	String LIST_STUDENTS_BY_COURSE_CODE = "SELECT * FROM Student "
		+ "WHERE course = ?";
	
	
	
	/* Delete Functions */
	// [D]elete Student
	String DELETE_STUDENT = "DELETE FROM Student "
		+ "WHERE id = ?";
	
	// [P]urge
	String DELETE_ALL = "DELETE FROM Student";
	
}
