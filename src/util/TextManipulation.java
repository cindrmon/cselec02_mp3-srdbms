package util;

public class TextManipulation {
	
	public static String toPascalCase(String inputStr) {
		
		String words[] = inputStr.split("\\s");
		String outputString = "";
		
		for(String w : words) {
			String firstLetter = w.substring(0,1);
			String restOfWord = w.substring(1);
			
			outputString += firstLetter.toUpperCase() + restOfWord + " ";
		}
		
		return outputString.trim();
		
	}
	
}
